# Sandwich_Hunter


## Installation guide:

1. Open android studio
2. Close any open project
3. Click "Get from VCS"
4. Use the HTTPS link for this repo
5. When the project is loaded:
File->close project
Click the three dots next to 'Get from VCS'
Select "Import project (Gradle, Eclipse ADT, etc.)"
Find your "sandwich_hunter" folder, and select its subfolder "SandwichHunter"

6. Go to the branch firebase
7. run


## HOWTO Play:


First insert a username, minimum 6 characters
You can create a new game by pressing "Create Game".
When someone else joins your game, you can start the game for all the players in the lobby
The game ends when someone loses, or someone finishes their sandwich. All players will then be brought to a result page

See the tutorial from the main menu for question on how to play.

If you instead want to join an existing game, click "Join Game". You'll then be able to join other games, if there are any.
