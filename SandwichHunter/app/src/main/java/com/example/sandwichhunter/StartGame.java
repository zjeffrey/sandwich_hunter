package com.example.sandwichhunter;

import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.SurfaceView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class StartGame extends AppCompatActivity {
    private GameView gameView;
    private MediaPlayer mediaPlayer;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get a Display object to access screen details
        Display display = getWindowManager().getDefaultDisplay();

        // Load the resolution into a Point object
        Point size = new Point();
        display.getSize(size);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Constants.SCREEN_WIDTH = dm.widthPixels;
        Constants.SCREEN_HEIGHT = dm.heightPixels;

        mediaPlayer = MediaPlayer.create(StartGame.this, R.raw.song);
        mediaPlayer.setLooping(true);
        //mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();

        // Initialize pongView and set it as the view
        gameView = new GameView(this, size.x, size.y);

        setContentView((SurfaceView)gameView);

        //goToMenu();
    }

    public void goToMenu() {
        Intent intent = new Intent(StartGame.this, MainActivity.class);
        startActivity(intent);
    }

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the pongView resume method to execute
        gameView.resume();
        mediaPlayer.start();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the pongView pause method to execute
        gameView.pause();
        mediaPlayer.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mediaPlayer.stop();
        mediaPlayer.release();
    }
}
