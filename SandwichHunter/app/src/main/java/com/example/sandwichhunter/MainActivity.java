package com.example.sandwichhunter;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private Button playButton;
    private Button joinButton;
    private Button tutorialButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playButton = findViewById(R.id.playButton);
        joinButton = findViewById(R.id.joinButton);
        tutorialButton = findViewById(R.id.tutorialButton);

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinLobby(view);
            }
        });

        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                joinGame(view);
            }
        });

        tutorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTutorial(view);
            }
        });
    }

    public void joinLobby(View view) {
        Intent intent = new Intent(MainActivity.this, LobbyActivity.class);
        startActivity(intent);
        //finish();
    }

    public void joinGame(View view) {
        Intent intent = new Intent(MainActivity.this, JoinActivity.class);
        startActivity(intent);
        //finish();
    }

    public void showTutorial(View view) {
        Intent intent = new Intent(MainActivity.this, TutorialActivity.class);
        startActivity(intent);
        //finish();
    }

}
