package com.example.sandwichhunter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class LobbyActivity extends AppCompatActivity {

    private Button backButton;
    private Button startGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);

        backButton = findViewById(R.id.backButton);
        startGameButton = findViewById(R.id.startGameButton);

        ProgressBar pgsBar = (ProgressBar)findViewById(R.id.pBar);
        //pgsBar.setVisibility(v.GONE);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LobbyActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        startGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LobbyActivity.this, StartGame.class);
                startActivity(intent);
            }
        });
    }
}